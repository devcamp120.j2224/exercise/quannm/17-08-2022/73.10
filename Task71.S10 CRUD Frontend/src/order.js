let gUserId = 0;
let gOrderId = 0;
// user
$.get(`http://localhost:8080/users`, loadUserToSelect);
$.get(`http://localhost:8080/orders`, loadOrderToTable);
let userSelectElement = $('#select-user');
function loadUserToSelect(paramUser) {
  paramUser.forEach((user) => {
    $('<option>', {
      text: user.hoTen,
      value: user.id,
    }).appendTo(userSelectElement);
  });
}
userSelectElement.change(onGetUserChange);
function onGetUserChange(event) {
  gUserId = event.target.value;
  if (gUserId == 0) {
    $.get(`http://localhost:8080/orders`, loadOrderToTable);
  } else {
    $.get(
      `http://localhost:8080/orders/${gUserId}`,
      loadOrderToTable,
    );
  }
}

let user = {
  newUser: {
    hoTen: '',
    email: '',
    soDienThoai: '',
    diaChi: '',
  },
  onCreateNewUserClick() {
    $('#modal-create-user').modal('show');
    console.log(gUserId);
  },
  onUpdateUserClick() {
    if (gUserId != 0) {
      $('#modal-create-user').modal('show');
      $.get(`http://localhost:8080/users/${gUserId}`, loadUserToInput);
    } else {
      alert('Please select a user to update');
    }
  },
  onSaveUserClick() {
    this.newUser = {
      hoTen: $('#input-fullname').val().trim(),
      email: $('#input-email').val().trim(),
      soDienThoai: $('#input-phone').val().trim(),
      diaChi: $('#input-address').val().trim(),
    };
    if (gUserId == 0) {
      if (validateUser(this.newUser)) {
        $.ajax({
          url: `http://localhost:8080/users`,
          method: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(this.newUser),
          success: () => {
            alert('successfully create new user');
            location.reload();
          },
          error: (err) => alert(err.responseText),
        });
      }
    } else {
      if (validateUser(this.newUser)) {
        $.ajax({
          url: `http://localhost:8080/users/${gUserId}`,
          method: 'PUT',
          contentType: 'application/json',
          data: JSON.stringify(this.newUser),
          success: () => {
            alert('successfully update user with id: ' + gUserId);
            location.reload();
          },
          error: (err) => alert(err.responseText),
        });
      }
    }
  },
  onDeleteUserClick() {
    if (gUserId != 0) {
      $('#modal-delete-user').modal('show');
    } else {
      alert('Please select a user to delete');
    }
  },
  onDeleteAllUserClick() {
    gUserId = 0;
    $('#modal-delete-user').modal('show');
  },
  onConfirmDeleteUserClick() {
    if (gUserId != 0) {
      $.ajax({
        url: `http://localhost:8080/users/${gUserId}`,
        method: 'delete',
        success: () => {
          alert('successfully delete user with id:' + gUserId);
          location.reload();
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://localhost:8080/users`,
        method: 'delete',
        success: () => {
          alert('successfully delete all users');
          location.reload();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};

$('#btn-create-user').click(user.onCreateNewUserClick);
$('#btn-update-user').click(user.onUpdateUserClick);
$('#btn-save-user').click(user.onSaveUserClick);
$('#btn-delete-user').click(user.onDeleteUserClick);
$('#btn-delete-all-User').click(user.onDeleteAllUserClick);
$('#btn-confirm-delete-user').click(user.onConfirmDeleteUserClick);

function loadUserToInput(paramUser) {
  $('#input-fullname').val(paramUser.hoTen);
  $('#input-email').val(paramUser.email);
  $('#input-phone').val(paramUser.soDienThoai);
  $('#input-address').val(paramUser.diaChi);
}

function validateUser(paramUser) {
  let vResult = true;
  try {
    if (paramUser.hoTen == '') {
      vResult = false;
      throw `full name can't be empty`;
    }

    if (paramUser.email == '') {
      vResult = false;
      throw ` email can't be empty`;
    }
    if (!validateEmail(paramUser.email)) {
      vResult = false;
      throw `must need right email`;
    }
    if (paramUser.soDienThoai == '') {
      vResult = false;
      throw `phone can't be empty`;
    }
    if (paramUser.phone.length < 10 || isNaN(paramUser.soDienThoai)) {
      vResult = false;
      throw `Cần nhập đúng kiểu số điện thoại phải không có chữ cái và đúng 10 số`;
    }
    if (paramUser.diaChi == '') {
      vResult = false;
      throw `Address can't be empty`;
    }
  } catch (e) {
    alert(e);
  }
  return vResult;
}

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
// order
let orderTable = $('#order-table').DataTable({
  columns: [
    { data: 'orderCode' },
    { data: 'kichCo' },
    { data: 'loaiPizza' },
    { data: 'voucher' },
    { data: 'thanhTien' },
    { data: 'Action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
});

function loadOrderToTable(paramOrder) {
  orderTable.clear();
  orderTable.rows.add(paramOrder);
  orderTable.draw();
}

let order = {
  newOrder: {
    orderCode: '',
    kichCo: '',
    loaiPizza: '',
    voucher: '',
    thanhTien: '',
  },
  onNewOrderClick() {
    gOrderId = 0;
    this.newOrder = {
      orderCode: $('#input-order-Code').val().trim(),
      kichCo: $('#input-pizza-size').val().trim(),
      loaiPizza: $('#input-pizza-type').val().trim(),
      voucher: $('#input-voucher').val().trim(),
      thanhTien: $('#input-Paid').val().trim(),
    };
    if (validateOrder(this.newOrder)) {
      if (gUserId == 0) {
        alert(`Please select customer to create a new order`);
      } else {
        $.ajax({
          url: `http://localhost:8080/orders/${gUserId}`,
          method: 'POST',
          data: JSON.stringify(this.newOrder),
          contentType: 'application/json',
          success: () => {
            alert(`Order created successfully`);
            $.get(
              `http://localhost:8080/orders/${gUserId}`,
              loadOrderToTable,
            );
            resetOrderInput();
          },
        });
      }
    }
  },
  onEditOrderClick() {
    vSelectedRow = $(this).parents('tr');
    vSelectedData = orderTable.row(vSelectedRow).data();
    console.log(vSelectedData);
    gOrderId = vSelectedData.id;
    loadOrderToInput(vSelectedData);
    // $.get(
    //   `http://localhost:8080/orders/${gOrderId}`,
    //   loadOrderToInput,
    // );
  },
  onUpdateOrderClick() {
    this.newOrder = {
      orderCode: $('#input-order-Code').val().trim(),
      kichCo: $('#input-pizza-size').val().trim(),
      loaiPizza: $('#input-pizza-type').val().trim(),
      voucher: $('#input-voucher').val().trim(),
      thanhTien: $('#input-Paid').val().trim(),
    };
    if (validateOrder(this.newOrder)) {
      if (gUserId == 0) {
        alert(`Please select customer to update a new order`);
      } else {
        $.ajax({
          url: `http://localhost:8080/orders/${gOrderId}`,
          method: 'PUT',
          data: JSON.stringify(this.newOrder),
          contentType: 'application/json',
          success: () => {
            alert(`Order updated successfully`);
            $.get(
              `http://localhost:8080/orders/${gOrderId}`,
              loadOrderToTable,
            );
            resetOrderInput();
          },
        });
      }
    }
  },
  onDeleteUserByIdClick() {
    $('#modal-delete-order').modal('show');
    vSelectedRow = $(this).parents('tr');
    vSelectedData = orderTable.row(vSelectedRow).data();
    gOrderId = vSelectedData.id;
  },
  onDeleteAllOrderClick() {
    $('#modal-delete-order').modal('show');
    gOrderId = 0;
  },
  onOrderConfirmDeleteClick() {
    if (gOrderId == 0) {
      $.ajax({
        url: `http://localhost:8080/orders`,
        method: 'delete',
        success: () => {
          alert('All Order was successfully deleted');
          $.get(`http://localhost:8080/orders`, loadOrderToTable);
          $('#modal-delete-order').modal('hide');
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://localhost:8080/orders/${gOrderId}`,
        method: 'delete',
        success: () => {
          alert(`Order with id ${gOrderId} was successfully deleted`);
          $.get(`http://localhost:8080/orders`, loadOrderToTable);
          $('#modal-delete-order').modal('hide');
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};

$('#create-order').click(order.onNewOrderClick);
$('#update-order').click(order.onUpdateOrderClick);
$('#delete-all-order').click(order.onDeleteAllOrderClick);
$('#btn-confirm-delete-order').click(order.onOrderConfirmDeleteClick);
$('#order-table').on('click', '.fa-edit', order.onEditOrderClick);
$('#order-table').on('click', '.fa-trash', order.onDeleteUserByIdClick);

function validateOrder(paramOrder) {
  let vResult = true;
  try {
    if (paramOrder.orderCode == '') {
      vResult = false;
      throw `Order code can't empty`;
    }
    if (paramOrder.kichCo == '') {
      vResult = false;
      throw `Pizza Size can't empty`;
    }
    if (paramOrder.loaiPizza == '') {
      vResult = false;
      throw `Pizza Type can't empty`;
    }
    if (paramOrder.thanhTien == '' || isNaN(paramOrder.thanhTien)) {
      vResult = false;
      throw `Paid cant' be empty or must is number`;
    }
  } catch (e) {
    alert(e);
  }
  return vResult;
}

function loadOrderToInput(paramOrder) {
  $('#input-order-Code').val(paramOrder.orderCode);
  $('#input-pizza-size').val(paramOrder.kichCo);
  $('#input-pizza-type').val(paramOrder.loaiPizza);
  $('#input-voucher').val(paramOrder.voucher);
  $('#input-Paid').val(paramOrder.thanhTien);
}

function resetOrderInput() {
  $('#input-order-Code').val('');
  $('#input-pizza-size').val('');
  $('#input-pizza-type').val('');
  $('#input-voucher').val('');
  $('#input-Paid').val('');
}
