package com.user_order.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "drinks")
public class Drink {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "ma_nuoc_uong")
    private String maNuocUong;
    
    @NotEmpty(message = "Nhập tên nước uống")
    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong;

    @NotNull(message = "Nhập đơn giá")
    @Column(name = "don_gia")
    private String donGia;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat")
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date ngayCapNhat;
    
    public Drink(int id, String maNuocUong, @NotEmpty(message = "Nhập tên nước uống") String tenNuocUong,
            @NotNull(message = "Nhập đơn giá") String donGia, String ghiChu, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public Drink() {
    }
    
        public int getId() {
            return id;
        }
    
        public void setId(int id) {
            this.id = id;
        }
    
        public String getMaNuocUong() {
            return maNuocUong;
        }
    
        public void setMaNuocUong(String maNuocUong) {
            this.maNuocUong = maNuocUong;
        }
    
        public String getTenNuocUong() {
            return tenNuocUong;
        }
    
        public void setTenNuocUong(String tenNuocUong) {
            this.tenNuocUong = tenNuocUong;
        }
    
        public String getDonGia() {
            return donGia;
        }
    
        public void setDonGia(String donGia) {
            this.donGia = donGia;
        }
    
        public String getGhiChu() {
            return ghiChu;
        }
    
        public void setGhiChu(String ghiChu) {
            this.ghiChu = ghiChu;
        }
    
        public Date getNgayTao() {
            return ngayTao;
        }
    
        public void setNgayTao(Date ngayTao) {
            this.ngayTao = ngayTao;
        }
    
        public Date getNgayCapNhat() {
            return ngayCapNhat;
        }
    
        public void setNgayCapNhat(Date ngayCapNhat) {
            this.ngayCapNhat = ngayCapNhat;
        }
    
    
}
