package com.user_order.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user_order.Model.User;

public interface IUserRepository extends JpaRepository<User, Integer>{
}
