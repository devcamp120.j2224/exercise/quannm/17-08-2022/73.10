package com.user_order.Repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user_order.Model.Order;


@Repository
public interface IOrderRepository extends JpaRepository<Order, Integer>{
}
