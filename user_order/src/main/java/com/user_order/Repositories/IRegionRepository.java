package com.user_order.Repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user_order.Model.Region;

public interface IRegionRepository extends JpaRepository<Region, Integer> {
    List<Region> findByCountryId(Integer countryId); 
    List<Region> findByCountryCountryCode(String countryCode);
    Optional<Region> findByIdAndCountryId(Integer id, Integer instructorId);
}
