package com.user_order.Controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.user_order.Model.User;
import com.user_order.Repositories.IUserRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class UserController {
    @Autowired
    IUserRepository userRepository;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUser(){
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable ("userId") int userId){
        try{
            Optional<User> userData = userRepository.findById(userId);
            if(userData.isPresent())
                return new ResponseEntity<>(userData.get(), HttpStatus.OK);
            else 
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user){
        try {
            Optional<User> userData = userRepository.findById(user.getId());
            if(userData.isPresent()){
                return ResponseEntity.unprocessableEntity().body("This id has already taken");
            } else {
                User newUser = new User();
                newUser.setId(user.getId());
                newUser.setDiaChi(user.getDiaChi());
                newUser.setEmail(user.getEmail());
                newUser.setHoTen(user.getHoTen());
                newUser.setSoDienThoai(user.getSoDienThoai());

                User saved = userRepository.save(newUser);
                return new ResponseEntity<>(saved, HttpStatus.CREATED);
            }
        } catch (Exception e) {
			return ResponseEntity.unprocessableEntity().body("Failed to Create user");
        }
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable ("userId") int userId, @Valid @RequestBody User user){
        try {
            Optional<User> userData = userRepository.findById(userId);
            if(userData.isPresent()){
                User newUser = userData.get();
                newUser.setId(userId);
                newUser.setDiaChi(user.getDiaChi());
                newUser.setEmail(user.getEmail());
                newUser.setHoTen(user.getHoTen());
                newUser.setSoDienThoai(user.getSoDienThoai());
                newUser.setOrders(userData.get().getOrders());

                User saved = userRepository.save(newUser);
                return new ResponseEntity<>(saved, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
			return ResponseEntity.unprocessableEntity().body("Failed to Create user");
        }
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable ("userId") int userId){
        if (userRepository.findById(userId).isPresent()){
            userRepository.deleteById(userId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
