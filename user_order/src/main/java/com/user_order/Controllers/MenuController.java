package com.user_order.Controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.Model.Menu;
import com.user_order.Repositories.IMenuRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class MenuController {
    @Autowired
    IMenuRepository pMenuRepository;

    @GetMapping("/menus")
    public ResponseEntity<List<Menu>> getAllMenu(){
        return new ResponseEntity<>(pMenuRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/menus/{menuId}")
    public ResponseEntity<Menu> getMenuById(@PathVariable ("menuId") int menuId){
        Optional<Menu> menuData = pMenuRepository.findById(menuId);
        if(menuData.isPresent())
            return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    }

    @PostMapping("/menus")
    public ResponseEntity<Object> createMenu(@Valid @RequestBody Menu Menu){
        try{
            Optional<Menu> MenuData = pMenuRepository.findById(Menu.getId());
            if(MenuData.isPresent()){
                return ResponseEntity.unprocessableEntity().body("This id has already taken.");
            } else {
                Menu newMenu = new Menu();
                newMenu.setDonGia(Menu.getDonGia());
                newMenu.setDuongKinh(Menu.getDuongKinh());
                newMenu.setSalad(Menu.getSalad());
                newMenu.setSoLuongNuoc(Menu.getSoLuongNuoc());
                newMenu.setSuon(Menu.getSuon());

                Menu saved = pMenuRepository.save(newMenu);
                return new ResponseEntity<>(saved, HttpStatus.CREATED);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/menus/{menuId}")
    public ResponseEntity<Object> updateMenu(@Valid @RequestBody Menu Menu, @PathVariable ("menuId") int menuId){
        try {
            Optional<Menu> MenuData = pMenuRepository.findById(menuId);
            if(MenuData.isPresent()){
                Menu newMenu = MenuData.get();

                newMenu.setId(MenuData.get().getId());
                newMenu.setDonGia(Menu.getDonGia());
                newMenu.setDuongKinh(Menu.getDuongKinh());
                newMenu.setSalad(Menu.getSalad());
                newMenu.setSoLuongNuoc(Menu.getSoLuongNuoc());
                newMenu.setSuon(Menu.getSuon());

                Menu saved = pMenuRepository.save(newMenu);
                return new ResponseEntity<>(saved, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @DeleteMapping("/menus/{menuId}")
    public ResponseEntity<Object> deleteMenu(@PathVariable ("menuId") int menuId){
        try {
            Optional<Menu> MenuData = pMenuRepository.findById(menuId);
            if(MenuData.isPresent()){
                pMenuRepository.deleteById(menuId);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

