package com.user_order.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.Model.Country;
import com.user_order.Repositories.ICountryRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CountryController {

    @Autowired
    ICountryRepository pCountryRepository;

    // GET ALL Country
    @GetMapping("/countries")
    public List<Country> getAllCountry() {
        return pCountryRepository.findAll();
    }

    // GET ID Country
    @GetMapping("/countries/{countryId}")
    public Country getCountryById(@PathVariable("countryId") int countryId) {
        if (pCountryRepository.findById(countryId).isPresent()) {
            return pCountryRepository.findById(countryId).get();
        } else {
            return null;
        }
    }

    // CREATE Country
    @PostMapping("/countries")
    public ResponseEntity<Object> createCountry(@RequestBody Country country) {
        try {
            Country newCountry = new Country();
            newCountry.setCountryCode(country.getCountryCode());
            newCountry.setCountryName(country.getCountryName());
            newCountry.setRegions(country.getRegions());
            Country savedCountry = pCountryRepository.save(newCountry);
            return new ResponseEntity<>(savedCountry, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // UPDATE Country
    @PutMapping("/countries/{countryId}")
    public ResponseEntity<Object> updateCountry(@PathVariable("countryId") int countryId, @RequestBody Country country) {
        Optional<Country> countryData = pCountryRepository.findById(countryId);
        if (countryData.isPresent()) {
            try {
                Country newCountry = countryData.get();
                newCountry.setCountryName(country.getCountryName());
                newCountry.setCountryCode(country.getCountryCode());
                newCountry.setRegions(country.getRegions());
                Country savedCountry = pCountryRepository.save(newCountry);
                return new ResponseEntity<>(savedCountry, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //DELETE ID Country
    @DeleteMapping("/countries/{countryId}")
    public ResponseEntity<Object> deleteCountryById(@PathVariable("countryId") int countryId){
        try {
            Optional<Country> countryData = pCountryRepository.findById(countryId);
            if (countryData.isPresent()) {
                pCountryRepository.deleteById(countryId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }  
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
