package com.user_order.Controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.Model.Drink;
import com.user_order.Repositories.IDrinkRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DrinkController {
    @Autowired
    IDrinkRepository pDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrink(){
        return new ResponseEntity<>(pDrinkRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/drinks/{drinkId}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable ("drinkId") int drinkId){
        Optional<Drink> drinkData = pDrinkRepository.findById(drinkId);
        if(drinkData.isPresent())
            return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    }

    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody Drink drink){
        try{
            Optional<Drink> drinkData = pDrinkRepository.findById(drink.getId());
            if(drinkData.isPresent()){
                return ResponseEntity.unprocessableEntity().body("This id has already taken.");
            } else {
                Drink newDrink = new Drink();
                newDrink.setMaNuocUong(drink.getMaNuocUong());
                newDrink.setTenNuocUong(drink.getTenNuocUong());
                newDrink.setDonGia(drink.getDonGia());
                newDrink.setGhiChu(drink.getGhiChu());
                newDrink.setNgayTao(new Date());

                Drink saved = pDrinkRepository.save(newDrink);
                return new ResponseEntity<>(saved, HttpStatus.CREATED);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drinks/{drinkId}")
    public ResponseEntity<Object> updateDrink(@Valid @RequestBody Drink drink, @PathVariable ("drinkId") int drinkId){
        try {
            Optional<Drink> drinkData = pDrinkRepository.findById(drinkId);
            if(drinkData.isPresent()){
                Drink newDrink = drinkData.get();
                
                newDrink.setMaNuocUong(drink.getMaNuocUong());
                newDrink.setTenNuocUong(drink.getTenNuocUong());
                newDrink.setDonGia(drink.getDonGia());
                newDrink.setGhiChu(drink.getGhiChu());
                newDrink.setNgayTao(drinkData.get().getNgayTao());
                newDrink.setId(drinkData.get().getId());
                newDrink.setNgayCapNhat(new Date());

                Drink saved = pDrinkRepository.save(newDrink);
                return new ResponseEntity<>(saved, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @DeleteMapping("/drinks/{drinkId}")
    public ResponseEntity<Object> deleteDrink(@PathVariable ("drinkId") int drinkId){
        try {
            Optional<Drink> drinkData = pDrinkRepository.findById(drinkId);
            if(drinkData.isPresent()){
                pDrinkRepository.deleteById(drinkId);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
