package com.user_order.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.user_order.Model.Order;
import com.user_order.Model.User;
import com.user_order.Repositories.IUserRepository;

@Service
public class UserService{

    public List<Order> getListOrdersByUserId (IUserRepository userRepository,int userId){
        List<Order> orders = new ArrayList<>();

        Optional<User> userData = userRepository.findById(userId);
        if(userData.isPresent()){
            User user = userData.get();
            user.getOrders().forEach(x -> {
                orders.add(x);
            });
        } 
        return orders;
    }
}